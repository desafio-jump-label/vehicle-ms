package br.well.eti.jump.vehiclems.unit;

import br.well.eti.jump.vehiclems.controllers.VehicleController;
import br.well.eti.jump.vehiclems.model.dto.request.VehicleRequest;
import br.well.eti.jump.vehiclems.model.dto.response.VehicleResponse;
import br.well.eti.jump.vehiclems.service.VehicleService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.List;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(value = VehicleController.class)
public class VehicleControllerTest {
    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private VehicleService service;

    private static final String ENDPOINT = "/vehicle";
    private static final Pageable PAGEABLE = PageRequest.of(0, 10);

    List<VehicleResponse> vehicleList = List.of(
            new VehicleResponse(1L, "JDP-9589", "Corsa Hacth", "GM", "Cinza", true),
            new VehicleResponse(2L, "ASD-1244", "Corsa Sedan", "GM", "Azul", true),
            new VehicleResponse(3L, "DFG-2365", "Corsa Wind", "GM", "Rosa", false),
            new VehicleResponse(4L, "JKG-4321", "Corsa Pickup", "GM", "Verde", true),
            new VehicleResponse(5L, "RTY-1258", "Novo Corsa", "GM", "Amarelo", true)
    );

    @Test
    public void retrieveAllVehicles() throws Exception {
        Page<VehicleResponse> vehicleResponsePage = new PageImpl<>(vehicleList, PAGEABLE, vehicleList.size());
        Mockito.when(service.getAll(PAGEABLE)).thenReturn(vehicleResponsePage);
        mockMvc.perform(
                MockMvcRequestBuilders.get(ENDPOINT + "?page=1&limit=10")
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void retrieveVehicleByPlate() throws Exception {
        VehicleResponse vehicleResponse = vehicleList.stream().filter(vehicle -> vehicle.getPlate().equals("JDP-9589")).collect(Collectors.toList()).get(0);
        Mockito.when(service.getByPlate("JDP9589")).thenReturn(vehicleResponse);
        mockMvc.perform(
                MockMvcRequestBuilders.get(ENDPOINT + "?filter=JDP-9589")
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void retrieveVehicleByStatus() throws Exception {
        List<VehicleResponse> vehicleResponse = vehicleList.stream().filter(vehicle -> vehicle.isStatus() == false).collect(Collectors.toList());
        Mockito.when(service.getByStatus(false)).thenReturn(vehicleResponse);
        mockMvc.perform(
                MockMvcRequestBuilders.get(ENDPOINT + "?filter=false")
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void retrieveVehicleById() throws Exception {
        VehicleResponse vehicleResponse = vehicleList.stream().filter(vehicle -> vehicle.getId() == 4).collect(Collectors.toList()).get(0);
        Mockito.when(service.getById(3L)).thenReturn(vehicleResponse);
        mockMvc.perform(
                MockMvcRequestBuilders.get(ENDPOINT + "/3")
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void createVehicle() throws Exception {
        VehicleRequest request = new VehicleRequest("POK-0673", "Novo Corsa NOVO", "GM", "Preto", true);

        ObjectMapper mapper = new ObjectMapper();
        String jsonString = mapper.writeValueAsString(request);

        RequestBuilder requestBuilder = MockMvcRequestBuilders.post(ENDPOINT).contentType(MediaType.APPLICATION_JSON_VALUE).accept(MediaType.APPLICATION_JSON_VALUE).content(jsonString);
        MvcResult result = mockMvc.perform(requestBuilder).andReturn();
        MockHttpServletResponse response = result.getResponse();
        assertEquals(HttpStatus.CREATED.value(), response.getStatus());
    }

    @Test
    public void updateVehicle() throws Exception {
        VehicleRequest request = new VehicleRequest("UWP-8564", "Novo Corsa", "GM", "Amarelo", true);

        ObjectMapper mapper = new ObjectMapper();
        String jsonString = mapper.writeValueAsString(request);

        RequestBuilder requestBuilder = MockMvcRequestBuilders.put(ENDPOINT + "/5").contentType(MediaType.APPLICATION_JSON_VALUE).accept(MediaType.APPLICATION_JSON_VALUE).content(jsonString);
        MvcResult result = mockMvc.perform(requestBuilder).andReturn();
        MockHttpServletResponse response = result.getResponse();
        assertEquals(HttpStatus.OK.value(), response.getStatus());
    }
}
