INSERT INTO vehicles (id, plate, model, manufacturer, color, status) VALUES
(1, 'PDY-5698', 'Corsa Hatch', 'GM', 'Preto', true),
(2, 'JSK-6047', 'Idea Essensse', 'FIAT', 'Azul', false),
(3, 'QWE-5698', 'Omega Hatch', 'GM', 'Amarelo', true),
(4, 'RTY-2396', 'Novo Fiesta', 'FIAT', 'Rosa', false),
(5, 'GHJ-1478', 'Palio Weekend', 'FIAT', 'Verde', true),
(6, 'XCV-0983', 'Vectra Sedan', 'GM', 'Preto', true),
(7, 'BNM-2345', 'Onix GLS', 'GM', 'Branco', false);