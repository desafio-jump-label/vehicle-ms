package br.well.eti.jump.vehiclems.service;

import br.well.eti.jump.vehiclems.exception.NotFoundException;
import br.well.eti.jump.vehiclems.mapper.VehicleMapper;
import br.well.eti.jump.vehiclems.model.Vehicle;
import br.well.eti.jump.vehiclems.model.dto.request.VehicleRequest;
import br.well.eti.jump.vehiclems.model.dto.response.VehicleResponse;
import br.well.eti.jump.vehiclems.repository.VehicleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class VehicleService {
    @Autowired
    private VehicleRepository repository;

    public Page<VehicleResponse> getAll(Pageable pageable){
        List<VehicleResponse> vehicleResponseList = repository.findAll(pageable).stream().map(VehicleMapper.INSTANCE::VehicleToResponse).collect(Collectors.toList());
        return new PageImpl<>(vehicleResponseList, pageable, vehicleResponseList.size());
    }

    public VehicleResponse getByPlate(String plate){
        return repository.findByPlate(plate).map(VehicleMapper.INSTANCE::VehicleToResponse).orElseThrow(() -> new NotFoundException(plate));
    }

    public List<VehicleResponse> getByStatus(boolean status){
        return repository.findByStatus(status).stream().map(VehicleMapper.INSTANCE::VehicleToResponse).collect(Collectors.toList());
    }

    public VehicleResponse getById(Long id){
        return repository.findById(id).map(VehicleMapper.INSTANCE::VehicleToResponse).orElseThrow(() -> new NotFoundException(id));
    }

    public VehicleResponse save(VehicleRequest request){
        Vehicle vehicle = repository.save(VehicleMapper.INSTANCE.requestToVehicle(request));
        return VehicleMapper.INSTANCE.VehicleToResponse(vehicle);
    }

    public VehicleResponse update(Long id, VehicleRequest request){
        return repository.findById(id)
                .map(vehicle -> {
                    vehicle.setColor(request.getColor());
                    vehicle.setManufacturer(request.getManufacturer());
                    vehicle.setModel(request.getModel());
                    vehicle.setStatus(request.isStatus());
                    return VehicleMapper.INSTANCE.VehicleToResponse(repository.save(vehicle));
                })
                .orElseThrow(() -> new NotFoundException(id));
    }

    public void delete(Long id){
        try{
            repository.deleteById(id);
        } catch (Exception e) {
            throw new NotFoundException(id);
        }

    }
}
