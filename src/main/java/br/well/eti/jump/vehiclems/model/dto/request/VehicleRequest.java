package br.well.eti.jump.vehiclems.model.dto.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@AllArgsConstructor
@RequiredArgsConstructor
@Getter
@Setter
public class VehicleRequest {
    @JsonProperty(value = "plate")
    @ApiModelProperty(value = "Placa do veículo", example = "JPD-6587")
    @NotNull(message = "Plate cannot be null")
    @Length(min=8, max=8, message="Plate must contain between 8 and 8 characters")
    private String plate;
    @JsonProperty(value = "model")
    @ApiModelProperty(value = "Modelo do veículo", example = "Corsa Hactch")
    @NotNull(message = "Model cannot be null")
    @Length(min=3, max=50, message="Model must contain between 3 and 50 characters")
    private String model;
    @JsonProperty(value = "manufacturer")
    @ApiModelProperty(value = "Fabricante do veículo", example = "GM")
    @NotNull(message = "Manufacturer cannot be null")
    @Length(min=2, max=50, message="Manufacturer must contain between 3 and 50 characters")
    private String manufacturer;
    @JsonProperty(value = "color")
    @ApiModelProperty(value = "Cor do veículo", example = "Azul")
    @NotNull(message = "Color cannot be null")
    @Length(min=3, max=50, message="Color must contain between 3 and 50 characters")
    private String color;
    @JsonProperty(value = "status")
    @ApiModelProperty(value = "Status do veículo", example = "true | false")
    @NotNull(message = "Status cannot be null")
    private boolean status;
}
