package br.well.eti.jump.vehiclems.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.http.HttpStatus;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class NotFoundException extends RuntimeException {
    private HttpStatus status_code = HttpStatus.NOT_FOUND;

    public NotFoundException(Long id) {
        super("Não foi possível encontrar registros com id:  " + id);
    }

    public NotFoundException(String plate) {
        super("Não foi possível encontrar registros com place:  " + plate);
    }

    public NotFoundException(boolean status) {
        super("Não foi possível encontrar registros com status:  " + status);
    }
}
