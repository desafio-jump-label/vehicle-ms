package br.well.eti.jump.vehiclems.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
@AllArgsConstructor
@RequiredArgsConstructor
@Table(name = "vehicles")
public class Vehicle {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", length = 4)
    private Long id;
    @Column(name = "plate", length = 8, nullable = false)
    private String plate;
    @Column(name = "model", length = 50, unique = true, nullable = false)
    private String model;
    @Column(name = "manufacturer", length = 50, nullable = false)
    private String manufacturer;
    @Column(name = "color", length = 10, nullable = false)
    private String color;
    @Column(name = "status", nullable = false)
    private boolean status;
}
