package br.well.eti.jump.vehiclems;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VehicleMsApplication {

    public static void main(String[] args) {
        SpringApplication.run(VehicleMsApplication.class, args);
    }

}
