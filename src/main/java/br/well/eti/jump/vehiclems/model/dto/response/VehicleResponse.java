package br.well.eti.jump.vehiclems.model.dto.response;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
@AllArgsConstructor
@RequiredArgsConstructor
@Getter
@Setter
public class VehicleResponse {
    @ApiModelProperty(value = "Id do veículo", example = "123")
    private Long id;
    @ApiModelProperty(value = "Placa do veículo", example = "JPD-6587")
    private String plate;
    @ApiModelProperty(value = "Modelo do veículo", example = "Corsa Hactch")
    private String model;
    @ApiModelProperty(value = "Fabricante do veículo", example = "GM")
    private String manufacturer;
    @ApiModelProperty(value = "Cor do veículo", example = "Azul")
    private String color;
    @ApiModelProperty(value = "Status do veículo", example = "true | false")
    private boolean status;
}
