package br.well.eti.jump.vehiclems.mapper;

import br.well.eti.jump.vehiclems.model.Vehicle;
import br.well.eti.jump.vehiclems.model.dto.request.VehicleRequest;
import br.well.eti.jump.vehiclems.model.dto.response.VehicleResponse;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface VehicleMapper {
    VehicleMapper INSTANCE = Mappers.getMapper(VehicleMapper.class);
    VehicleResponse VehicleToResponse(Vehicle Vehicle);
    Vehicle requestToVehicle(VehicleRequest VehicleRequest);

}
