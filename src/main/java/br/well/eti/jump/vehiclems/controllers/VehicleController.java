package br.well.eti.jump.vehiclems.controllers;

import br.well.eti.jump.vehiclems.model.dto.request.VehicleRequest;
import br.well.eti.jump.vehiclems.model.dto.response.VehicleResponse;
import br.well.eti.jump.vehiclems.service.VehicleService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.Positive;
import java.util.List;

@RestController
@RequestMapping("/vehicle")
public class VehicleController {
    @Autowired
    private VehicleService service;

    @GetMapping
    @ApiOperation(value = "Get all Vehicles")
    public ResponseEntity<Page<VehicleResponse>> getAll(Pageable pageable){
        return ResponseEntity.ok().body(service.getAll(pageable));
    }

    @GetMapping("/filter={plate:[A-Za-z-\0-9]+}")
    @ApiOperation(value = "Get Vehicle by plate")
    public ResponseEntity<VehicleResponse> getByPlate(@Valid @PathVariable String plate){

        return ResponseEntity.ok().body(service.getByPlate(plate));
    }

    @GetMapping("/filter={status:[0-1]}")
    @ApiOperation(value = "Get Vehicle by status")
    public ResponseEntity<List<VehicleResponse>> getByPlate(@Valid @PathVariable boolean status){
        return ResponseEntity.ok().body(service.getByStatus(status));
    }

    @GetMapping("/{id}")
    @ApiOperation(value = "Get Vehicle by id")
    public ResponseEntity<VehicleResponse> getById(@Valid @Positive @PathVariable Long id){
        return ResponseEntity.ok().body(service.getById(id));
    }

    @PostMapping
    @ApiOperation(value = "Save Vehicle")
    public ResponseEntity<VehicleResponse> save(@RequestBody VehicleRequest body){
        return ResponseEntity.status(HttpStatus.CREATED).body(service.save(body));
    }

    @PutMapping("/{id}")
    @ApiOperation(value = "Update Vehicle")
    public ResponseEntity<VehicleResponse> update(@Positive @PathVariable Long id, @Valid @RequestBody VehicleRequest body){
        return ResponseEntity.ok().body(service.update(id, body));
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "Delete Vehicle by id")
    public ResponseEntity delete(@Positive @PathVariable Long id){
        service.delete(id);
        return ResponseEntity.ok().build();
    }

}
