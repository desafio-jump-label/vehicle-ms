# vehicle-ms (Desafio Jump Label)

## Build Setup
```
# Run application
mvn spring-boot:run

# Endpoint URI
http://localhost:8887

# Swagger JSON
http://localhost:8887/v2/api-docs

# Swagger Documentation
http://localhost:8887/swagger-ui.html

```